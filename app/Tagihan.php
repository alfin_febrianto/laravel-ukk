<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Tagihan extends Model {

    public $timestamps = false;
    protected $table = 'tagihan';
    protected $fillable = ['id_penggunaan','jumlah_meter','jumlah_tagihan','status'];
    protected $primaryKey = 'id_tagihan';
    protected $guarded = ['id_tagihan'];

    public function penggunaan(){
        return $this->belongsTo('App\Penggunaan','id_penggunaan');
    }

    public function pembayaran(){
        return $this->hasMany('App\Pembayaran');
    }
}
