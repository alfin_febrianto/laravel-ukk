<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model {

		protected $table = 'pelanggan';
		protected $fillable = ['no_meter','nama','user','password','alamat','id_tarif'];
    protected $primaryKey = 'id_pelanggan';
    protected $guarded = ['id_pelanggan'];
    public $timestamps = false;

	public function rules($id_pelanggan)
	{
		return [
            'no_meter' => 'required|numeric|unique:pelanggan,no_meter,'.$id_pelanggan.',id_pelanggan',
            'nama' => 'required|min:2',
            'username' => 'required|min:4|unique:pelanggan,username,'.$id_pelanggan.',id_pelanggan',
            'password' => 'required_if:id_pelanggan,==,""|min:4',
            'alamat' => '',
            'id_tarif' => 'required'
        ];
	}

	public function attributes(){
	    return [
	        'id_tarif' => 'Tarif'
        ];
    }

	public function login($req)
	{
		return Pelanggan::where('username',$req->username)->where('password',md5($req->password))->first();
	}

    public function tarif(){
        return $this->belongsTo('App\Tarif','id_tarif');
    }

    public function penggunaan(){
        return $this->hasMany('App\Penggunaan','id_pelanggan');
    }

}
