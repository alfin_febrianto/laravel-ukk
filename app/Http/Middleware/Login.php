<?php namespace App\Http\Middleware;

use Closure;
use Session;

class Login{
    public function handle($request, Closure $next)
    {
        if(!Session::get('login')){
            return redirect('login')->with('fail','You must login first');
        }else{
            return $next($request);
        }
    }
}
