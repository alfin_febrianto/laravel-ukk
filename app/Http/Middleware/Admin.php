<?php namespace App\Http\Middleware;

use Closure;
use Session;

class Admin{
    public function handle($request, Closure $next)
    {
        if(!Session::get('admin')){
            return redirect('home');
        }else{
            return $next($request);
        }
    }

}

?>
