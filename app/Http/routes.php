<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::get('/login', function(){
	if(Session::get('login')) {
		return redirect('/home');
	} else {
		return view('login.login', ['action' => 'login']);
	}
});

Route::get('/login/admin', function(){
	if(Session::get('login')) {
		return redirect('/home');
	} else {
		return view('login.login', ['action' => '/login/admin']);
	}
});

Route::get('/logout', function(){
	Session::flush();
	return redirect('login');
});

Route::post('/login', 'AuthController@login');
Route::post('/login/admin','AuthController@loginAdmin');
