<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pelanggan;
use App\Admin;
use Validator;
use Session;

class AuthController extends Controller{

  public function login(Request $req){

        $pelanggan = new Pelanggan();
        $validator = Validator::make($req->all(),[
            'username' => 'required|min:4',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            if (count($pelanggan->login($req))>0) {
                $data_pelanggan = Pelanggan::where('username','=',$req->username)->first();
                $session = array(
                    'login' => true,
                    'user.id' => $data_pelanggan->id_pelanggan,
                    'user.username' => $data_pelanggan->username,
                    'user.nama' => $data_pelanggan->nama,
                );
                Session::put($session);
                $data['success'] = true;
                $data['redirect']= '/home';
                return response()->json($data);
                // return redirect('home');
            }else{
                $data['false'] = true;
                $data['false_msg']= 'Wrong username or password!';
                return response()->json($data);
            }
        }
    }


    public function loginAdmin(Request $req){

        $admin = new Admin(); //model admin
        $validator = Validator::make($req->all(),[
            'username' => 'required|min:4',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            $data['error'] = true;
            $data['error_msg']= $validator->errors();
            return response()->json($data);
        }else{
            if (count($admin->login($req))>0) {
                $data_admin = Admin::where('username','=',$req->username)->first();
                $session = array(
                    'login' => true,
                    'admin' => true,
                    'user.id' => $data_admin->id_admin,
                    'user.username' => $data_admin->username,
                    'user.nama' => $data_admin->nama_admin,
                    'user.id_level'=> $data_admin->id_level,
                );
                Session::put($session);
                $data['success'] = true;
                $data['redirect']= '/home';
                return response()->json($data);
                // return redirect('home');
            }else{
                $data['false'] = true;
                $data['false_msg']= 'Wrong username or password!';
                return response()->json($data);
            }
        }
    }


}


?>
