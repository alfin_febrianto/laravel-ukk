<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Philbert I Fast build Admin dashboard for any platform</title>
		<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>

		<!-- Favicon -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">

		<!-- vector map CSS -->
		<link href="/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

		<link href="{{asset('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css">
		<!-- Custom CSS -->
		<link href="/dist/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->

		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="/index.html">
						<img class="brand-img mr-10" src="/dist/img/logo.png" alt="brand"/>
						<span class="brand-text">Philbert</span>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">Don't have an account?</span>
					<a class="inline-block btn btn-info btn-success btn-rounded btn-outline" href="/signup.html">Sign Up</a>
				</div>
				<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<h3 class="text-center txt-dark mb-10">Sign in to Philbert</h3>
											<h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
										</div>
										<div class="form-wrap">
											<div class="alert" id="alert"></div>
											<form method="POST" action="{{ url($action) }}" id="form-login">
											<input type="hidden" value="{{ csrf_token() }}" name="_token">
												<div class="form-group">
													<label class="control-label mb-10" >Username</label>
													<input id="username" type="text" name="username" class="form-control" placeholder="Username">
													<div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
												</div>
												<div class="form-group">
													<label class="pull-left control-label mb-10">Password</label>
													<input id="password" type="password" name="password" class="form-control" placeholder="Password" autocomplete="Password">
													<div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
												</div>
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-success btn-rounded">sign in</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
				</div>

			</div>
			<!-- /Main Content -->

		</div>
		<!-- /#wrapper -->

		<!-- JavaScript -->

		<!-- jQuery -->
		<script src="/vendors/bower_components/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

		<!-- SweetAlert -->
		<script src="{{asset('vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>

		<!-- Slimscroll JavaScript -->
		<script src="/dist/js/jquery.slimscroll.js"></script>

		<!-- Init JavaScript -->
		<script src="/dist/js/init.js"></script>

		<script src="/dist/js/irfanValidator.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#form-login').submit(function(e){
						e.preventDefault();
						var fa = $(this);
						$('#form-login').find(':submit').attr("disabled", "disabled");
						$('#form-login').find(':submit').html("Menyimpan...");
						$.ajax({
								type: "POST",
								url: fa.attr('action'),
								data: $("#form-login").serialize(),
								dataType: "json",
								success: function(data){
										if (data.success == true) {
												window.location.href = data.redirect;
										}else if (data.error == true) {
												$('#form-login').find(':submit').attr("disabled", false);
												$('#form-login').find(':submit').html("Simpan");
												$.each(data.error_msg, function(key, value) {
														if (value) {
																console.log(key);
																$('#' + key).parents('.form-group').removeClass('has-success');
																$('#' + key).parents('.form-group').addClass('has-error has-danger');
																$('#' + key).parents('.form-group').find('#error').html(value);
														}else{
																$('#edit-' + key).parents('.form-group').addClass('has-success');
														}

												});
										}else if(data.false == true){
												swal({
														title: 'Error',
														text: data.false_msg,
														type: 'warning',
														timer : 2000,
														showCancelButton: false,
														showConfirmButton: false
												});
												$('#form-login').find(':submit').attr("disabled", false);
												$('#form-login').find(':submit').html("Simpan");
										}
								}
						});
				});
			});
		</script>

		@include('sweet::alert')
	</body>
</html>
