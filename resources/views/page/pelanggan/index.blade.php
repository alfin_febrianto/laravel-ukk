@extends('layout/template')
@section('content')
    <h3>Pelanggan</h3>
    @if (Session::has('type'))
        <div class="alert alert-{!! Session::get('type') !!} alert-dismissable" id="alert">
            <p class="pull-left">{!! Session::get('message') !!}</p>
            <div class="clearfix"></div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default card-view">
                <form action="{{url('pelanggan/add')}}" method="post" id="form-add">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="form-group">
                        <label class="control-label mb-10 text-left">No Meter</label>
                        <input type="number" class="form-control" id="no_meter" placeholder="421350"  name="no_meter">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Nama</label>
                        <input type="text" class="form-control" id="nama"  placeholder="Irfan Hakim" name="nama">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Username</label>
                        <input type="text" class="form-control" id="username"  placeholder="irfanhkm_" name="username">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Password</label>
                        <input type="password" class="form-control" id="password"  placeholder="*****" name="password">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat"></textarea>
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Daya</label>
                        <select class="form-control" name="id_tarif" id="id_tarif">
                            <option value="" style="display: none;">Pilih Daya</option>
                            @foreach($dataTarif as $tarif)
                                <option value="{{$tarif->id_tarif}}">{{$tarif->daya}} / Rp.{{$tarif->perkwh}}</option>
                            @endforeach
                        </select>
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <button type="submit" class="btn cur-p btn-primary">Tambah</button>
                        </div>
                    </div>
                </form>
                <br>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default card-view">
                <table class="table table-hover">
                    <tr>
                        <th>No</th><th>No Meter</th><th>Nama</th><th>Username</th><th>Daya</th><th>Aksi</th>
                    </tr>
                    <?php $i=1;?>
                    @foreach($dataAll as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->no_meter}}</td>
                        <td>{{$data->nama}}</td>
                        <td>{{$data->username}}</td>
                        <td>{{$data->tarif->daya}} / Rp.{{$data->tarif->perkwh}}</td>
                        <td>
                            <a style="float: left;margin-right: 1%;" href="#" data-toggle="modal" data-target="#modal"
                               onclick="edit({{$data->id_pelanggan}},{{$data->no_meter}},'{{$data->nama}}','{{$data->username}}','{{$data->alamat}}',{{$data->id_tarif}})" class="btn btn-primary">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <form action="{{url('pelanggan/delete')}}" method="post" style="margin: 0;padding: 0;float: left" class="delete">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                <input type="hidden" value="{{ $data->id_pelanggan }}" name="id_pelanggan">
                                <button  type="submit" class="btn btn-danger btn-delete" >
                                    <span class="fa fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div id="modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit</h5>
                </div>
                <div class="modal-body">
                    <div class="help-block with-errors" id="alert"></div>
                    <form action="{{url('pelanggan/edit')}}" method="POST" id="form-edit">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">No Meter</label>
                            <input type="number" class="form-control" id="edit-no_meter" placeholder="421350"  name="no_meter">
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10">Nama</label>
                            <input type="text" class="form-control" id="edit-nama"  placeholder="Irfan Hakim" name="nama">
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10">Username</label>
                            <input type="text" class="form-control" id="edit-username"  placeholder="irfanhkm_" name="username">
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10">Alamat</label>
                            <textarea class="form-control" name="alamat" id="edit-alamat"></textarea>
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10">Daya</label>
                            <select class="form-control" name="id_tarif" id="edit-id_tarif">
                                <option value="" style="display: none;">Pilih Daya</option>
                                @foreach($dataTarif as $tarif)
                                    <option value="{{$tarif->id_tarif}}">{{$tarif->daya}} / Rp.{{$tarif->perkwh}}</option>
                                @endforeach
                            </select>
                            <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                            <input type="hidden" id="edit-id_pelanggan" name="id_pelanggan">
                        </div>
                        <button type="submit" class="btn cur-p btn-primary">Edit</button>
                        <button type="button" onclick="closeModal()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @if (Session::has('type'))
    <script>
        $("#alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });
    </script>
    @endif
    <script>
        function edit(id,no_meter,nama,username,alamat,id_tarif) {
            $('#edit-id_pelanggan').val(id);
            $('#edit-no_meter').val(no_meter);
            $('#edit-nama').val(nama);
            $('#edit-username').val(username);
            $('#edit-alamat').val(alamat);
            $('#edit-id_tarif').val(id_tarif);
        }
    </script>
    <script src="{{asset('dist/js/irfanValidator.js')}}"></script>
@endsection