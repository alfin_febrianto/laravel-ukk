@extends('layout/template')
@section('content')
    <h3>Penggunaan</h3>
    @if (Session::has('type'))
        <div class="alert alert-{!! Session::get('type') !!} alert-dismissable" id="alert">
            <p class="pull-left">{!! Session::get('message') !!}</p>
            <div class="clearfix"></div>
        </div>
    @endif
    <br>
    <a href="{{url($backUrl)}}" class="btn btn-primary">Back</a><br>
    @if (count($dataAll)>0)
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default card-view">
                <table class="table table-hover">
                    <tr>
                        <th>No</th><th>Bulan</th><th>Tahun</th><th>Meter Awal, Meter Akhir</th><th>Penggunaan</th><th>Jumlah Tagihan</th><th>Status</th><th>Aksi</th>
                    </tr>
                    <?php $i=1;?>
                    @foreach($dataAll as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{config("params.bulan.".$data->penggunaan->bulan."")}}</td>
                        <td>{{$data->penggunaan->tahun}}</td>
                        <td>{{$data->penggunaan->meteran_awal}}, {{$data->penggunaan->meteran_akhir}}</td>
                        <td>{{$data->penggunaan->meteran_akhir - $data->penggunaan->meteran_awal}}</td>
                        <td>Rp {{($data->penggunaan->meteran_akhir - $data->penggunaan->meteran_awal)*$data->penggunaan->pelanggan->tarif->perkwh}}</td>
                        <td>{{$data->status}}</td>
                        @if ($data->status == "Belum Bayar" && \Request::session()->get('user.id_level')==1)
                            <td>
                                <form action="{{url('penggunaan/delete')}}" method="post" style="margin: 0;padding: 0;float: left" class="delete">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <input type="hidden" value="{{ $data->status }}" name="status">
                                    <input type="hidden" value="{{ $data->id_penggunaan }}" name="id_penggunaan">
                                    <input type="hidden" value="{{ $data->id_tagihan }}" name="id_tagihan">
                                    <button  type="submit" class="btn btn-danger btn-delete" >
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </form>
                            </td>
                        @else
                            <td>-</td>
                        @endif
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    @else
        <center><h3>Data tidak ditemukan</h3></center>
    @endif
@endsection
@section('js')
    @if (Session::has('type'))
    <script>
        $("#alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });
    </script>
    @endif
    <script src="{{asset('dist/js/irfanValidator.js')}}"></script>
@endsection