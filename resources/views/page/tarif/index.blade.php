@extends('layout/template')
@section('content')
    <h3>Tarif</h3>
    @if (Session::has('type'))
        <div class="alert alert-{!! Session::get('type') !!} alert-dismissable" id="alert">
            <p class="pull-left">{!! Session::get('message') !!}</p>
            <div class="clearfix"></div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default card-view">
                <form action="{{url('tarif/add')}}" method="post" id="form-add">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="form-group">
                        <label class="control-label mb-10 text-left">Daya</label>
                        <input type="number" class="form-control" id="daya" placeholder="450"  name="daya">
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10">Per/KWH</label>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input type="number" class="form-control" id="perkwh"  placeholder="1500" name="perkwh">
                        </div>
                        <div style="text-transform: capitalize" class="help-block with-errors" id="error"></div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <button type="submit" class="btn cur-p btn-primary">Tambah</button>
                        </div>
                    </div>
                </form>
                <br>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default card-view">
                <table class="table table-hover">
                    <tr>
                        <th>No</th><th>Daya</th><th>Per/KWH</th><th>Aksi</th>
                    </tr>
                    <?php $i=1;?>
                    @foreach($dataAll as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->daya}}</td>
                        <td>{{$data->perkwh}}</td>
                        <td>
                            <a style="float: left;margin-right: 1%;" href="#" data-toggle="modal" data-target="#modal" onclick="edit({{$data->id_tarif}},{{$data->daya}},{{$data->perkwh}})" class="btn btn-primary">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <form action="{{url('tarif/delete')}}" method="post" style="margin: 0;padding: 0;float: left" class="delete">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                <input type="hidden" value="{{ $data->id_tarif }}" name="id_tarif">
                                <button  type="submit" class="btn btn-danger btn-delete" >
                                    <span class="fa fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div id="modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Edit</h5>
                </div>
                <div class="modal-body">
                    <div class="help-block with-errors" id="alert"></div>
                    <form action="{{url('tarif/edit')}}" method="POST" id="form-edit">
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        <div class="form-group">
                            <label class="control-label mb-10 text-left">Daya</label>
                            <input type="text" class="form-control" id="edit-daya" placeholder="450"  name="daya">
                            <div class="help-block with-errors" id="error">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label mb-10" for="exampleInputuname_1">Per/KWH</label>
                            <div class="input-group">
                                <div class="input-group-addon">Rp.</div>
                                <input type="text" class="form-control" id="edit-perkwh"  placeholder="1500" name="perkwh">
                            </div>
                            <div class="help-block with-errors" id="error">
                            </div>
                            <input type="hidden" name="id_tarif" class="form-control" id="edit-id_tarif">
                        </div>
                        <button type="submit" class="btn cur-p btn-primary">Edit</button>
                        <button type="button" onclick="closeModal()" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @if (Session::has('type'))
    <script>
        $("#alert").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });
    </script>
    @endif
    <script>
        function edit(id,daya,perkwh) {
            $('#edit-id_tarif').val(id);
            $('#edit-daya').val(daya);
            $('#edit-perkwh').val(perkwh);
        }
    </script>
    <script src="{{asset('dist/js/irfanValidator.js')}}"></script>
@endsection