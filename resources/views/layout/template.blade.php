<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Philbert I Fast build Admin dashboard for any platform</title>
	<meta name="description" content="Philbert is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Philbert Admin, Philbertadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

	<link href="{{asset('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css">

	<!-- Custom CSS -->
	<link href="{{asset('dist/css/style.css')}}" rel="stylesheet" type="text/css">
</head>

<body>
	<!-- Preloader -->
	<!-- <div class="preloader-it">
		<div class="la-anim-1"></div>
	</div> -->
	<!-- /Preloader -->
	<div class="wrapper theme-1-active pimary-color-green">
		<!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="{{url('home')}}">
							<img class="brand-img" src="{{asset('dist/img/logo.png')}}" alt="brand"/>
							<span class="brand-text">Philbert</span>
						</a>
					</div>
				</div>
				<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
				<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"></a>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
			</div>
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">
					<li class="dropdown auth-drp">
						<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src="{{asset('dist/img/user1.png')}}" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
						<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
							<li>
								<a href="{{url('logout')}}"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
		<!-- /Top Menu Items -->
		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span></span>
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a class="{{ (Request::segment(1) == 'home' || Request::segment(1) == '' || Request::segment(1) == '#') ? 'active' : '' }}" href="{{url('home')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="ti-dashboard mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
				</li>
				@if($navbar)
					@if(\Request::session()->get('user.id_level')==1)
					<li>
						<a class="{{ (Request::segment(1) == 'tarif') ? 'active' : '' }}" href="{{url('tarif')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="ti-plug mr-20"></i><span class="right-nav-text">Tarif</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="{{ (Request::segment(1) == 'admin') ? 'active' : '' }}" href="{{url('admin')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="fa fa-meh-o mr-20"></i><span class="right-nav-text">Admin</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="{{ (Request::segment(1) == 'pelanggan') ? 'active' : '' }}" href="{{url('pelanggan')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="ti-user mr-20"></i><span class="right-nav-text">Pelanggan</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="{{ (Request::segment(1) == 'penggunaan') ? 'active' : '' }}" href="{{url('penggunaan')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="fa fa-sticky-note mr-20"></i><span class="right-nav-text">Penggunaan</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					</li>
					@else
					<li>
						<a class="{{ (Request::segment(1) == 'verifikasi') ? 'active' : '' }}" href="{{url('verifikasi')}}" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="ti-check-box mr-20"></i><span class="right-nav-text">Verifikasi Pembayaran</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
					</li>
					@endif
				@endif
			</ul>
		</div>
		<!-- /Left Sidebar Menu -->
		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">
							<?php if (isset($judul)): ?>
							<?=$judul?>
							<?php else: ?>

							<?php endif ?>
						</h5>
					</div>
					<!-- Breadcrumb -->
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						@yield('content')

					</div>

					<!-- Footer -->
					<footer class="footer container-fluid pl-30 pr-30">
						<div class="row">
							<div class="col-sm-12">
								<p>2017 &copy; Philbert. Pampered by Hencework</p>
							</div>
						</div>
					</footer>
					<!-- /Footer -->

				</div>
				<!-- /Main Content -->

			</div>
		</div>
    </div>
		<!-- /#wrapper -->

		<!-- JavaScript -->

		<!-- jQuery -->
		<script src="{{asset('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
		<script src="{{asset('dist/js/jquery.slimscroll.js')}}"></script>

		<script src="{{asset('vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="{{asset('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

		<script src="{{asset('vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

		<!-- Data table JavaScript -->
		<script src="{{asset('vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>

		<script src="{{asset('dist/js/init.js')}}"></script>

		@yield('js')
		@include('sweet::alert')

	</body>
</html>
