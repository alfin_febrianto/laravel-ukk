$(document).ready(function() {
    $('.btn-delete').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        swal({
                title             : "Apakah anda yakin ?",
                text              : "Data yang dihapus tidak dapat kembali",
                type              : "warning",
                showCancelButton  : true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText : "Ya",
                cancelButtonText  : "Tidak",
                closeOnConfirm    : false,
                closeOnCancel     : false
            },
            function(isConfirm){
                if(isConfirm){
                    self.parents(".delete").submit();
                }
                else{
                    swal.close();
                }
            });
    });
    $('#error').html(" ");
    $('#form-add').submit(function(e){
        e.preventDefault();
        var fa = $(this);
        $('#form-add').find(':submit').attr("disabled", "disabled");
        $('#form-add').find(':submit').html("Menyimpan...");
        $.ajax({
            type: "POST",
            url: fa.attr('action'),
            data: $("#form-add").serialize(),
            dataType: "json",
            success: function(data){
                if (data.success == true) {
                    swal({
                        title: 'Berhasil',
                        text: 'Data Berhasil Ditambahkan',
                        type: 'success',
                        showCancelButton: false,
                        showConfirmButton: false
                    });
                    setTimeout(function () {
                        window.location.href = data.redirect; //will redirect to your blog page (an ex: blog.html)
                    }, 2000); //will call the function after 2 secs.
                }else if (data.error == true) {
                    $('#form-add').find(':submit').attr("disabled", false);
                    $('#form-add').find(':submit').html("Simpan");
                    $.each(data.error_msg, function(key, value) {
                        if (value) {
                            console.log(key);
                            $('#' + key).parents('.form-group').removeClass('has-success');
                            $('#' + key).parents('.form-group').addClass('has-error has-danger');
                            $('#' + key).parents('.form-group').find('#error').html(value);
                        }else{
                                $('#' + key).parents('.form-group').addClass('has-success');
                        }

                    });
                }else if(data.false == true){
                    swal({
                        title: 'Error',
                        text: data.false_msg,
                        type: 'warning',
                        timer : 2000,
                        showCancelButton: false,
                        showConfirmButton: false
                    });
                    $('#form-add').find(':submit').attr("disabled", false);
                    $('#form-add').find(':submit').html("Simpan");
                }
            }
        });
    });
    $('#error').html(" ");
    $('#form-edit').submit(function(e){
        e.preventDefault();
        var fa = $(this);
        $('#form-edit').find(':submit').attr("disabled", "disabled");
        $('#form-edit').find(':submit').html("Menyimpan...");
        $.ajax({
            type: "POST",
            url: fa.attr('action'),
            data: $("#form-edit").serialize(),
            dataType: "json",
            success: function(data){
                if (data.success == true) {
                    swal({
                        title: 'Berhasil',
                        text: 'Data Berhasil Diubah',
                        type: 'success',
                        showCancelButton: false,
                        showConfirmButton: false
                    });
                    setTimeout(function () {
                        window.location.href = data.redirect; //will redirect to your blog page (an ex: blog.html)
                    }, 2000); //will call the function after 2 secs.
                }else if (data.error == true) {
                    $('#form-edit').find(':submit').attr("disabled", false);
                    $('#form-edit').find(':submit').html("Simpan");
                    $.each(data.error_msg, function(key, value) {
                        if (value) {
                            console.log(key);
                            $('#edit-' + key).parents('.form-group').removeClass('has-success');
                            $('#edit-' + key).parents('.form-group').addClass('has-error has-danger');
                            $('#edit-' + key).parents('.form-group').find('#error').html(value);
                        }else{
                            $('#edit-' + key).parents('.form-group').addClass('has-success');
                        }

                    });
                }else if(data.false == true){
                    swal({
                        title: 'Error',
                        text: data.false_msg,
                        type: 'warning',
                        timer : 2000,
                        showCancelButton: false,
                        showConfirmButton: false
                    });
                    $('#form-edit').find(':submit').attr("disabled", false);
                    $('#form-edit').find(':submit').html("Simpan");
                    $('#alert').addClass('alert-danger');
                }
            }
        });
    });
    $('#error').html(" ");
    $('#form-add input').on('keyup', function () {
        $('#alert').removeClass('alert-danger');
        $('#alert').html("");
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-edit input').on('keyup', function () {
        $('#alert').removeClass('alert-danger');
        $('#alert').html("");
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-login input').on('keyup', function () {
        $('#alert').removeClass('alert-danger');
        $('#alert').html("");
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-add textarea').on('keyup', function () {
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-edit textarea').on('keyup', function () {
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-login textarea').on('keyup', function () {
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-add select').on('change', function () {
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-edit select').on('change', function () {
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

    $('#form-login select').on('change', function () {
        $(this).parents('.form-group').removeClass('has-error has-danger').addClass('has-success');
        $(this).parents('.form-group').find('#error').html(" ");
    });

});
